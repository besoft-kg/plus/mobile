import React from 'react';
import {Card, Colors, Avatar, Paragraph, Text} from 'react-native-paper';
import BaseScreen from '../screens/BaseScreen';
import {verticalScale, moderateScale, scale} from 'react-native-size-matters';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {View} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {inject, observer, Observer} from 'mobx-react';
import {useNavigation} from '@react-navigation/core';

@inject('store')
@observer
class PostItemComponent extends BaseScreen {
  render() {
    const {item} = this.props;
    const user = this.userStore.items.get(item.user_id);

    return (
      <>
        <Card style={{marginBottom: verticalScale(8)}}>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() =>
              this.navigation.navigate('ProfileViewScreen', {id: user.id})
            }>
            <Card.Title
              title={user.full_name}
              subtitle={`@${user.login} | ${item.ui_distance}`}
              left={(props) => (
                <Observer>
                  {() => (
                    <Avatar.Image source={user.picture_source} {...props} />
                  )}
                </Observer>
              )}
            />
          </TouchableOpacity>
          {/*<Card.Cover source={{uri: 'https://picsum.photos/700'}} />*/}
          {item.content && (
            <Card.Content style={{marginTop: verticalScale(8)}}>
              <Paragraph>{item.content}</Paragraph>
            </Card.Content>
          )}
          <Card.Actions>
            <View
              flex={1}
              flexDirection={'row'}
              alignItems={'center'}
              justifyContent={'space-between'}>
              <View style={{flexDirection: 'row'}}>
                <TouchableOpacity
                  style={{marginRight: 12}}
                  activeOpacity={0.6}
                  onPress={() => item.like()}>
                  <MaterialCommunityIcons
                    style={{marginRight: scale(4)}}
                    name={item.liked ? 'heart' : 'heart-outline'}
                    color={item.liked ? Colors.red800 : Colors.grey800}
                    size={moderateScale(28)}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  activeOpacity={0.6}
                  onPress={() =>
                    this.navigation.navigate('PostViewScreen', {id: item.id})
                  }
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <MaterialCommunityIcons
                    style={{marginRight: scale(4)}}
                    name="comment-outline"
                    color={Colors.grey800}
                    size={moderateScale(28)}
                  />
                  <Text>{item.comments_count}</Text>
                </TouchableOpacity>
              </View>
              <View
                flexDirection={'row'}
                justifyContent={'center'}
                alignItems={'center'}>
                <TouchableOpacity
                  onPress={() =>
                    this.navigation.navigate('PostViewScreen', {id: item.id})
                  }
                  style={{
                    marginRight: 12,
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <MaterialCommunityIcons
                    style={{marginRight: scale(4)}}
                    name="eye-outline"
                    color={Colors.grey800}
                    size={moderateScale(28)}
                  />
                  <Text>{item.views}</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => item.toggleSave()}
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <MaterialCommunityIcons
                    name={item.saved ? 'bookmark' : 'bookmark-outline'}
                    color={Colors.grey800}
                    size={moderateScale(28)}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </Card.Actions>
          <Card.Content>
            <View
              flexDirection={'row'}
              flex={1}
              justifyContent={'space-between'}
              alignItems={'center'}>
              <Text style={{color: Colors.grey500}}>
                {item.likes_count > 0 ? (
                  <>
                    Нравится:{' '}
                    <Text style={{fontWeight: '500', color: Colors.grey800}}>
                      {item.likes_count}
                    </Text>
                  </>
                ) : null}
              </Text>
              <Text style={{color: Colors.grey500}}>{item.ui_created_at}</Text>
            </View>
          </Card.Content>
        </Card>
      </>
    );
  }
}

export default function (props) {
  const navigation = useNavigation();

  return <PostItemComponent {...props} navigation={navigation} />;
}
