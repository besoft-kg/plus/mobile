import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {Alert, View} from 'react-native';
import {scale} from 'react-native-size-matters';
import {
  ActivityIndicator,
  Avatar,
  Colors,
  Divider,
  FAB,
  List,
} from 'react-native-paper';
import ActionSheet from 'react-native-actions-sheet/src/index';
import React, {useRef, useState} from 'react';
import {observer} from 'mobx-react';
import {useStore} from '../stores';
import requester from '../utils/requester';

export default observer(function ProfilePictureComponent() {
  const store = useStore();
  const actionSheetRef = useRef();
  const [picture, setPicture] = useState(null);
  const [loading, setLoading] = useState(false);

  const options = {
    mediaType: 'photo',
    maxWidth: 500,
    maxHeight: 500,
    quality: 1,
  };

  const setNewPicture = (pic) => {
    if (loading) {
      return;
    }
    setPicture(pic);
    actionSheetRef.current?.setModalVisible();
    setLoading(true);
    requester
      .post('profile/picture', {
        picture: {
          name: pic.fileName,
          type: pic.type,
          uri: pic.uri,
        },
      })
      .then(({data}) => {
        if (data.status === 'success') {
          store.userStore.createOrUpdate(data.payload);
        }
      })
      .catch((e) => {
        console.dir(e);
      })
      .finally(() => {
        setPicture(null);
        setLoading(false);
      });
  };

  const launchPicker = (type) => {
    if (loading) {
      return;
    }

    const callback = (res) => {
      const assets = res.assets || [];
      if (!res.didCancel && assets.length > 0) {
        setNewPicture(assets[0]);
      }
    };

    if (type === 'camera') {
      launchCamera(
        {
          ...options,
        },
        callback,
      );
    } else {
      launchImageLibrary(
        {
          ...options,
        },
        callback,
      );
    }
  };

  const _delete = () => {
    if (loading) {
      return;
    }

    setLoading(true);
    setPicture(null);

    requester
      .post('profile/picture')
      .then(({data}) => {
        if (data.status === 'success') {
          store.userStore.createOrUpdate(data.payload);
        }
      })
      .catch((e) => {
        console.dir(e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _deleteConfirm = () => {
    actionSheetRef.current?.setModalVisible();
    Alert.alert('Подтвердите', 'Вы точно хотите удалить?', [
      {
        text: 'Нет',
      },
      {text: 'Да', onPress: () => _delete()},
    ]);
  };

  return (
    <>
      <ActionSheet ref={actionSheetRef}>
        <View style={{margin: scale(8)}}>
          <List.Item
            onPress={() => launchPicker('camera')}
            title="Сделать фото"
            left={(props) => <List.Icon {...props} icon="camera" />}
          />
          <Divider />
          <List.Item
            onPress={() => launchPicker('library')}
            title="Выбрать из галарее"
            left={(props) => <List.Icon {...props} icon="library" />}
          />
          {store.appStore.user.picture ? (
            <>
              <Divider />
              <List.Item
                onPress={() => _deleteConfirm()}
                title="Удалить фото"
                left={(props) => <List.Icon {...props} icon="delete" />}
              />
            </>
          ) : null}
        </View>
      </ActionSheet>

      <View style={{position: 'relative'}}>
        {loading ? (
          <ActivityIndicator
            size={20}
            style={{
              position: 'absolute',
              zIndex: 10,
              alignSelf: 'center',
              top: 50,
            }}
          />
        ) : (
          <FAB
            style={{
              borderRadius: 100,
              position: 'absolute',
              right: -2,
              bottom: -2,
              zIndex: 10,
              elevation: 10,
              shadowColor: Colors.black,
              shadowRadius: 5,
              padding: 2,
              backgroundColor: Colors.green500,
            }}
            color={Colors.white}
            small
            icon="camera"
            onPress={() => actionSheetRef.current?.setModalVisible()}
          />
        )}
        <Avatar.Image
          style={loading ? {opacity: 0.4, backgroundColor: '#eee'} : {}}
          size={120}
          source={
            picture ? {uri: picture.uri} : store.appStore.user.picture_source
          }
        />
      </View>
    </>
  );
});
