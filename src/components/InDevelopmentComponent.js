import {Button, Text, Title} from 'react-native-paper';
import {View} from 'react-native';
import React from 'react';
import {moderateScale, verticalScale} from 'react-native-size-matters';
import {useNavigation} from '@react-navigation/core';

export default function InDevelopmentComponent() {
  const navigation = useNavigation();

  return (
    <View
      style={{
        flex: 1,
        margin: moderateScale(8),
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Text style={{fontSize: moderateScale(45)}}>😣</Text>
      <Title style={{marginTop: verticalScale(8)}}>Sorry!</Title>
      <Text
        style={{
          color: '#808080',
          textAlign: 'center',
          marginTop: verticalScale(8),
        }}>
        Данный раздел находится в стадии разработки (изменения, дополнения).
      </Text>
      <Text
        style={{
          color: '#808080',
          textAlign: 'center',
          marginTop: verticalScale(8),
        }}>
        Приносим свои извинения за временное неудобство
      </Text>
      <Button
        onPress={() => navigation.goBack()}
        style={{marginTop: verticalScale(16)}}
        mode={'contained'}>
        Окей, ничего страшного! 😉
      </Button>
    </View>
  );
}
