import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import BaseScreen from '../screens/BaseScreen';
import NotificationsScreen from '../screens/NotificationsScreen';

const Stack = createStackNavigator();

class NotificationsStack extends BaseScreen {
  render() {
    return (
      <Stack.Navigator initialRouteName={'NotificationsScreen'}>
        <Stack.Screen
          options={{
            title: 'Уведомления',
          }}
          name={'NotificationsScreen'}
          component={NotificationsScreen}
        />
      </Stack.Navigator>
    );
  }
}

export default NotificationsStack;
