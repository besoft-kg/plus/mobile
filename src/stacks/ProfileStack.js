import React from 'react';
import BaseScreen from '../screens/BaseScreen';

import {createStackNavigator} from '@react-navigation/stack';
import ProfileScreen from '../screens/ProfileScreen';
import AboutUsScreen from '../screens/AboutUsScreen';
import EditProfileScreen from '../screens/EditProfileScreen';
import LikedPostsScreen from '../screens/LikedPostsScreen';
import MyPostsScreen from '../screens/MyPostsScreen';
import {withTranslation} from 'react-i18next';
import SavedPostsScreen from '../screens/SavedPostsScreen';

const Stack = createStackNavigator();

@withTranslation()
class ProfileStack extends BaseScreen {
  render() {
    return (
      <Stack.Navigator initialRouteName={'ProfileScreen'}>
        <Stack.Screen
          options={{
            headerShown: false,
            title: this.t('profile:profile'),
          }}
          name={'ProfileScreen'}
          component={ProfileScreen}
        />
        <Stack.Screen
          options={{
            title: this.t('profile:about_app'),
          }}
          name={'AboutUsScreen'}
          component={AboutUsScreen}
        />
        <Stack.Screen
          options={{
            title: this.t('profile:edit_profile'),
          }}
          name={'EditProfileScreen'}
          component={EditProfileScreen}
        />
        <Stack.Screen
          options={{
            title: this.t('profile:liked_posts'),
          }}
          name={'LikedPostsScreen'}
          component={LikedPostsScreen}
        />
        <Stack.Screen
          options={{
            title: this.t('profile:saved_posts'),
          }}
          name={'SavedPostsScreen'}
          component={SavedPostsScreen}
        />
        <Stack.Screen
          options={{
            title: this.t('profile:my_posts'),
          }}
          name={'MyPostsScreen'}
          component={MyPostsScreen}
        />

        {/*<Stack.Screen*/}
        {/*  options={{*/}
        {/*    title: 'Политика конфиденциальности',*/}
        {/*  }}*/}
        {/*  name={'PrivacyPolicyScreen'}*/}
        {/*  component={PrivacyPolicyScreen}*/}
        {/*/>*/}

        {/*<Stack.Screen*/}
        {/*  options={{*/}
        {/*    title: 'Ползовательское соглашения',*/}
        {/*  }}*/}
        {/*  name={'UserTermsScreen'}*/}
        {/*  component={UserTermsScreen}*/}
        {/*/>*/}
      </Stack.Navigator>
    );
  }
}

export default ProfileStack;
