import React from 'react';
import BaseScreen from '../screens/BaseScreen';
import {createStackNavigator} from '@react-navigation/stack';
import NewPostScreen from '../screens/NewPostScreen';
import AttachMediaScreen from '../screens/AttachMediaScreen';

const Stack = createStackNavigator();

class NewPostStack extends BaseScreen {
  render() {
    return (
      <Stack.Navigator initialRouteName={'NewPostScreen'}>
        <Stack.Screen
          options={{
            headerShown: false,
          }}
          name={'NewPostScreen'}
          component={NewPostScreen}
        />
        <Stack.Screen
          options={{
            headerShown: false,
          }}
          name={'AttachMediaScreen'}
          component={AttachMediaScreen}
        />
      </Stack.Navigator>
    );
  }
}

export default NewPostStack;
