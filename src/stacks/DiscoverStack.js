import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import BaseScreen from '../screens/BaseScreen';
import DiscoverScreen from '../screens/DiscoverScreen';

const Stack = createStackNavigator();

class DiscoverStack extends BaseScreen {
  render() {
    return (
      <Stack.Navigator initialRouteName={'DiscoverScreen'}>
        <Stack.Screen
          options={{
            headerShown: false,
          }}
          name={'Discover'}
          component={DiscoverScreen}
        />
      </Stack.Navigator>
    );
  }
}

export default DiscoverStack;
