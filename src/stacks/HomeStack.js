import React from 'react';
import BaseScreen from '../screens/BaseScreen';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from '../screens/HomeScreen';
import PostViewScreen from '../screens/PostViewScreen';
import ProfileViewScreen from '../screens/ProfileViewScreen';

const Stack = createStackNavigator();

class HomeStack extends BaseScreen {
  render() {
    return (
      <Stack.Navigator initialRouteName={'HomeScreen'}>
        <Stack.Screen
          options={{
            headerShown: false,
            title: 'Главная',
          }}
          name={'HomeScreen'}
          component={HomeScreen}
        />
        <Stack.Screen
          options={{
            title: 'Пост',
          }}
          name={'PostViewScreen'}
          component={PostViewScreen}
        />
        <Stack.Screen
          options={{
            title: 'Пользователь',
            headerShown: false,
          }}
          name={'ProfileViewScreen'}
          component={ProfileViewScreen}
        />
      </Stack.Navigator>
    );
  }
}

export default HomeStack;
