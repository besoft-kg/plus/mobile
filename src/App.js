import React from 'react';
// import {
//   statusCodes,
//   GoogleSignin,
//   GoogleSigninButton,
// } from '@react-native-google-signin/google-signin';
import {createStackNavigator} from '@react-navigation/stack';
import AuthScreen from './screens/AuthScreen';
import auth from '@react-native-firebase/auth';
import {Alert, Platform, View} from 'react-native';
import {Text} from 'react-native-paper';
import {inject, observer} from 'mobx-react';
import BaseScreen from './screens/BaseScreen';
import storage from './utils/storage';
import requester from './utils/requester';
import 'moment/locale/ru';
import 'moment/locale/ky';
import {APP_VERSION} from './utils/settings';
import messaging from '@react-native-firebase/messaging';
import NewUserScreen from './screens/NewUserScreen';
import MainScreen from './screens/MainScreen';

const Stack = createStackNavigator();

@inject('store')
@observer
class App extends BaseScreen {
  onTokenRefreshDisposer = null;

  // signIn = async () => {
  //   try {
  //     const userInfo = await GoogleSignin.signIn();
  //     this.setState({userInfo});
  //     console.log(userInfo);
  //
  //     const googleCredential = auth.GoogleAuthProvider.credential(
  //       userInfo.idToken,
  //     );
  //     console.log(googleCredential);
  //     console.log(await auth().signInWithCredential(googleCredential));
  //   } catch (error) {
  //     if (error.code === statusCodes.SIGN_IN_CANCELLED) {
  //       alert('SIGN_IN_CANCELLED');
  //     } else if (error.code === statusCodes.IN_PROGRESS) {
  //       // operation (e.g. sign in) is in progress already
  //       alert('IN_PROGRESS');
  //     } else {
  //       // some other error happened
  //     }
  //   }
  // };

  makeAuthRequest = async (id_token, token) => {
    return await requester.post('auth', {
      id_token,
      platform: Platform.OS === 'ios' ? 'ios' : 'android',
      version_code: APP_VERSION,
      language: this.appStore.language,
      fcm_token: token,
    });
  };

  componentDidMount() {
    this.unsubscribe = auth().onAuthStateChanged(async (user) => {
      try {
        const language = await storage.get('language', 'ru');
        this.appStore.setLanguage(language, false);
        this.appStore.setValue('distance', await storage.get('distance', 0));
        if (user) {
          const id_token = await user.getIdToken();
          const {data} = await this.makeAuthRequest(
            id_token,
            await messaging().getToken(),
          );

          this.appStore.signIn(data.payload.user, id_token);

          this.onTokenRefreshDisposer = messaging().onTokenRefresh(
            async (token) => {
              await this.makeAuthRequest(id_token, token);
            },
          );
        } else {
          await this.appStore.clearUser();
        }
        this.appStore.setValue('app_is_ready', true);
      } catch (e) {
        Alert.alert('Ошибка!', e.message);
        console.dir(e);
      }
    });
  }

  componentWillUnmount() {
    this.unsubscribe() && this.unsubscribe();
    this.onTokenRefreshDisposer && this.onTokenRefreshDisposer();
  }

  render() {
    if (!this.appStore.app_is_ready) {
      return (
        <View flex={1} justifyContent={'center'} alignItems={'center'}>
          <Text style={{fontSize: 54}}>⏳</Text>
        </View>
      );
    }

    return (
      <>
        {/*<GoogleSigninButton*/}
        {/*  style={{width: 192, height: 48}}*/}
        {/*  size={GoogleSigninButton.Size.Wide}*/}
        {/*  color={GoogleSigninButton.Color.Dark}*/}
        {/*  onPress={this.signIn}*/}
        {/*  disabled={this.state.isSigninInProgress}*/}
        {/*/>*/}
        <Stack.Navigator screenOptions={{headerShown: false}}>
          {this.appStore.authenticated ? (
            <>
              {this.appStore.user.is_new ? (
                <Stack.Screen
                  name={'NewUserScreen'}
                  component={NewUserScreen}
                />
              ) : (
                <Stack.Screen name={'MainScreen'} component={MainScreen} />
              )}
            </>
          ) : (
            <Stack.Screen name={'AuthScreen'} component={AuthScreen} />
          )}
        </Stack.Navigator>
      </>
    );
  }
}

export default App;
