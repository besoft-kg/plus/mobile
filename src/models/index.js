import {types as t} from 'mobx-state-tree';

export const float = t.custom({
  name: 'float',
  fromSnapshot(value) {
    return Number.parseFloat(value);
  },
  toSnapshot(value) {
    return value;
  },
  isTargetType(value) {
    return Number(value) === value && value % 1 !== 0;
  },
  getValidationMessage(value) {
    if (/^-?\d+\.\d+$/.test(value)) {
      return '';
    }
    return `'${value}' doesn't look like a valid float number`;
  },
});

export const makeEditable = () => {
  return t
    .model({})
    .views((self) => ({}))
    .actions((self) => {
      return {
        setValue: (name, value) => (self[name] = value),
        setValues: (s) => Object.keys(s).map((v) => (self[v] = s[v])),
      };
    });
};

export const dateConverter = (d) => {
  if (!d) {
    return null;
  }
  if (d instanceof Date) {
    return d;
  } else if (typeof d === 'number' || typeof d === 'string') {
    return new Date(d);
  } else if ('toDate' in d) {
    return d.toDate();
  }
  return null;
};
