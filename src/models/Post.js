import {types as t, flow, getRoot, getSnapshot} from 'mobx-state-tree';
import {dateConverter, float, makeEditable} from './index';
import requester from '../utils/requester';
import PostComment from './PostComment';
import moment from 'moment';

const post = t
  .model('post', {
    id: t.identifierNumber,
    user_id: t.integer,
    content: t.maybeNull(t.string),
    views: t.integer,
    distance: float,
    created_at: t.Date,
    updated_at: t.Date,
    liked: t.optional(t.boolean, false),
    likes_count: t.integer,
    comments: t.optional(t.map(PostComment), {}),
    comments_count: t.integer,
  })
  .views((self) => ({
    get ui_distance() {
      return `${self.distance || '~'} км`;
    },
    get ui_created_at() {
      return moment(self.created_at)
        .locale(self.root.appStore.language)
        .fromNow();
      //return moment(self.created_at).format('DD-MM-YYYY HH:mm');
    },
    get root() {
      return getRoot(self);
    },
    get saved() {
      return getSnapshot(self.root.postStore.saved).includes(self.id);
    },
    get is_not_sync() {
      return self.id < 1;
    },
  }))
  .actions((self) => {
    const like = flow(function* () {
      try {
        self.liked = !self.liked;
        self.likes_count += self.liked ? 1 : -1;
        const {data} = yield requester.post('post/like', {
          post_id: self.id,
        });
        if (data.payload.liked !== self.liked) {
          self.liked = data.liked;
          self.likes_count += data.liked ? 1 : -1;
        }
      } catch (e) {
        self.liked = !self.liked;
        self.likes_count += self.liked ? 1 : -1;
        console.dir(e);
      }
    });

    const setComment = (v, user_id) => {
      if ('user' in v) {
        self.root.userStore.createOrUpdate(v.user);
      }

      self.comments.set(v.id, {
        ...v,
        post_id: +v.post_id,
        user_id: 'user' in v ? v.user.id : user_id,
        created_at: dateConverter(v.created_at),
        updated_at: dateConverter(v.updated_at),
      });
    };

    const setComments = (g) => {
      g.map((v) => self.setComment(v));
    };

    const toggleSave = () => {
      self.root.postStore.toggleSave(self.id);
    };

    const postComment = flow(function* (content, user_id) {
      if (self.id < 1 || !content) {
        return;
      }

      let random_id = 0;

      while (self.comments.get(random_id)) {
        random_id--;
      }

      self.setComment(
        {
          id: random_id,
          user_id,
          post_id: self.id,
          content,
          created_at: new Date(),
          updated_at: new Date(),
        },
        user_id,
      );
      self.comments_count++;

      try {
        const {data} = yield requester.post('post/comment', {
          content: content,
          post_id: self.id,
        });
        self.setComment(data.payload, user_id);
      } catch (e) {
        self.comments_count--;
      } finally {
        self.comments.delete(random_id);
      }
    });

    return {
      postComment,
      like,
      setComment,
      setComments,
      toggleSave,
    };
  });

export default t.compose(post, makeEditable());
