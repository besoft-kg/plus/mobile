import {types as t} from 'mobx-state-tree';
import {makeEditable} from './index';

const post_comment = t
  .model('post_comment', {
    id: t.identifierNumber,
    user_id: t.integer,
    post_id: t.integer,
    content: t.maybeNull(t.string),
    created_at: t.Date,
    updated_at: t.Date,
  })
  .views((self) => ({
    get is_not_sync() {
      return self.id < 1;
    },
  }));

export default t.compose(post_comment, makeEditable());
