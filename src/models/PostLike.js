import {types as t} from 'mobx-state-tree';
import {makeEditable} from './index';
import Post from './Post';
import User from './User';

const post_like = t.model('post_like', {
  id: t.identifierNumber,
  user_id: t.integer,
  post_id: t.integer,
  post: t.maybeNull(t.reference(Post)),
  user: t.maybeNull(t.reference(User)),
  created_at: t.Date,
  updated_at: t.Date,
});

export default t.compose(post_like, makeEditable());
