import {types as t} from 'mobx-state-tree';
import {makeEditable} from './index';

const image = t.model('image', {
  id: t.identifierNumber,
  name: t.string,
  hash: t.string,
  path: t.map(t.string),
  size: t.map(t.integer),
  dimensions: t.map(t.map(t.integer)),
  url: t.frozen({
    original: t.string,
  }),
  extension: t.string,
  created_at: t.Date,
  updated_at: t.Date,
});

export default t.compose(image, makeEditable());
