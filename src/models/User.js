import {types as t} from 'mobx-state-tree';
import {float, makeEditable} from './index';
import Post from './Post';
import Image from './Image';

const user = t
  .model('user', {
    id: t.identifierNumber,
    provider: t.maybeNull(t.string),
    provider_id: t.maybeNull(t.string),
    email: t.maybeNull(t.string),
    login: t.maybeNull(t.string),
    full_name: t.maybeNull(t.string),
    bio: t.maybeNull(t.string),
    abilities: t.maybeNull(t.array(t.string)),
    picture_id: t.maybeNull(t.integer),
    picture: t.maybeNull(Image),
    last_action: t.Date,
    language: t.maybeNull(t.enumeration(['ru', 'ky'])),
    settings: t.maybeNull(t.frozen()),
    location_lat: t.maybeNull(float),
    location_lng: t.maybeNull(float),
    location_last_refresh: t.maybeNull(t.Date),
    created_at: t.maybeNull(t.Date),
    updated_at: t.maybeNull(t.Date),
    posts: t.optional(t.array(t.reference(Post)), []),
  })
  .views((self) => ({
    get is_new() {
      if (!self.login || !self.full_name) {
        return true;
      }
      return self.login.length === 0 || self.full_name.length === 0;
    },
    get picture_source() {
      return self.picture_id && self.picture
        ? {uri: self.picture?.url.original}
        : require('../assets/avatar.jpg');
    },
  }))
  .actions((self) => {
    const addPost = (id) => {
      if (!self.posts.includes(id)) {
        return;
      }
      self.posts.push(id);
    };

    return {
      addPost,
    };
  });

export default t.compose(user, makeEditable());
