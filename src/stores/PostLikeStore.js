import {getRoot, types as t} from 'mobx-state-tree';
import {dateConverter, makeEditable} from '../models';
import PostLike from '../models/PostLike';

const PostLikeStore = t
  .model('PostLikeStore', {
    items: t.optional(t.map(PostLike), {}),
    liked: t.optional(t.array(t.reference(PostLike)), []),
  })
  .actions((self) => {
    const createOrUpdate = (item) => {
      if (Array.isArray(item)) {
        item.map((v) => self.createOrUpdate(v));
        return;
      }

      if ('post' in item) {
        self.root.postStore.createOrUpdate(item.post);
      }
      if ('user' in item) {
        self.root.userStore.createOrUpdate(item.user);
      }

      self.items.set(item.id, {
        ...item,
        post: 'post' in item ? item.post.id : null,
        user: 'user' in item ? item.user.id : null,
        created_at: dateConverter(item.created_at),
        updated_at: dateConverter(item.updated_at),
      });
    };

    const remove = (id) => {
      self.items.delete(id);
    };

    return {
      createOrUpdate,
      remove,
    };
  })
  .views((self) => ({
    get root() {
      return getRoot(self);
    },
  }));

export default t.compose(PostLikeStore, makeEditable());
