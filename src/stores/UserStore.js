import {getRoot, types as t} from 'mobx-state-tree';
import User from '../models/User';
import {dateConverter} from '../models';

const UserStore = t
  .model('UserStore', {
    items: t.optional(t.map(User), {}),
  })
  .actions((self) => {
    const createOrUpdate = (item) => {
      if (Array.isArray(item)) {
        item.map((v) => self.createOrUpdate(v));
        return;
      }

      const picture = item.picture
        ? {
            ...item.picture,
            created_at: new Date(item.picture.created_at),
            updated_at: new Date(item.picture.updated_at),
          }
        : null;

      self.items.set(item.id, {
        ...(self.items.get(item.id) || {}),
        ...item,
        picture,
        created_at: dateConverter(item.created_at),
        updated_at: dateConverter(item.updated_at),
        last_action: dateConverter(item.last_action),
        location_last_refresh: item.location_last_refresh
          ? dateConverter(item.location_last_refresh)
          : null,
      });
    };

    const remove = (id) => {
      self.items.delete(id);
    };

    return {
      createOrUpdate,
      remove,
    };
  })
  .views((self) => ({
    get root() {
      return getRoot(self);
    },
  }));

export default UserStore;
