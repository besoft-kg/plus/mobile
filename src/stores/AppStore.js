import {getRoot, types as t} from 'mobx-state-tree';
import User from '../models/User';
import {makeEditable} from '../models';
import storage from '../utils/storage';
import auth from '@react-native-firebase/auth';
import i18next from 'i18next';
import requester from '../utils/requester';
import moment from 'moment';
import {Alert} from 'react-native';

const AppStore = t
  .model('AppStore', {
    user: t.maybeNull(t.reference(User)),
    token: t.maybeNull(t.string),
    distance: t.optional(t.integer, 0),

    messages: t.array(
      t.frozen({
        variant: t.string,
        content: t.string,
      }),
    ),

    app_is_ready: t.optional(t.boolean, false),
    language: t.optional(t.enumeration(['ky', 'ru']), 'ru'),
  })
  .actions((self) => ({
    signIn: (user, token = null) => {
      self.setUser(user, token);
      storage.set('user', user).then(() => {});
    },

    setUser: (user, token = null) => {
      self.root.userStore.createOrUpdate(user);
      self.user = user.id;
      self.setLanguage(user.language);
      if (token) {
        self.token = token;
      }
    },

    setLanguage: (lng, _storage = true) => {
      self.language = lng;
      i18next.changeLanguage(lng).then();
      moment.locale(lng);
      if (_storage) {
        storage.set('language', lng).then();
        if (self.authenticated && self.user.language !== lng) {
          self.user.setValue('language', lng);
          requester
            .post('profile', {
              language: lng,
            })
            .then()
            .catch();
        }
      }
    },

    signOut: async () => {
      await auth().signOut();
    },

    clearUser: async () => {
      self.user = null;
      self.token = null;
      await storage.remove('user');
    },

    showMessage: (content, variant = 'default') => {
      Alert.alert('', content);
    },

    showError: (content) => {
      self.showMessage(content, 'error');
    },

    showWarning: (content) => {
      self.showMessage(content, 'warning');
    },

    showSuccess: (content) => {
      self.showMessage(content, 'success');
    },

    showInfo: (content) => {
      self.showMessage(content, 'info');
    },
  }))
  .views((self) => ({
    get root() {
      return getRoot(self);
    },

    get authenticated() {
      return self.user !== null;
    },

    get ui_language() {
      if (self.language === 'ky') {
        return 'Кыргызча';
      } else {
        return 'Русский';
      }
    },
  }));

export default t.compose(AppStore, makeEditable());
