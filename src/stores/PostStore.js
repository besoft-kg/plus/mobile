import {
  flow,
  getRoot,
  getSnapshot,
  onSnapshot,
  types as t,
} from 'mobx-state-tree';
import Post from '../models/Post';
import {dateConverter, makeEditable} from '../models';
import storage from '../utils/storage';

const PostStore = t
  .model('PostStore', {
    items: t.optional(t.map(Post), {}),
    home: t.optional(t.array(t.reference(Post)), []),
    saved: t.optional(t.array(t.reference(Post)), []),
  })
  .actions((self) => {
    let onSnapshotDisposer = null;

    const afterCreate = flow(function* () {
      const saved = yield storage.get('saved', []);
      self.createOrUpdate(saved);
      self.saved = saved.map((g) => g.id);

      onSnapshotDisposer = onSnapshot(
        self.saved,
        flow(function* (snapshot) {
          yield storage.set(
            'saved',
            snapshot.map((g) => getSnapshot(self.items.get(g))),
          );
        }),
      );
    });

    const beforeDestroy = () => {
      onSnapshotDisposer && onSnapshotDisposer();
    };

    const createOrUpdate = (item) => {
      if (Array.isArray(item)) {
        item.map((v) => self.createOrUpdate(v));
        return;
      }

      let user;

      if ('user' in item) {
        user = self.root.userStore.createOrUpdate(item.user);
      }

      self.items.set(item.id, {
        ...item,
        comments: {},
        distance: (item.distance || 0).toFixed(2),
        liked: item.liked > 0,
        created_at: dateConverter(item.created_at),
        updated_at: dateConverter(item.updated_at),
      });

      user?.addPost(item.id);

      if (item.comments?.length > 0) {
        self.items.get(item.id).setComments(item.comments);
      }
    };

    const remove = (id) => {
      self.items.delete(id);
    };

    const toggleSave = (id) => {
      const item = self.items.get(id);
      if (!item) {
        return;
      }
      const snapshot = getSnapshot(self.saved);
      if (!snapshot.includes(id)) {
        self.saved.push(id);
      } else {
        const index = snapshot.indexOf(id);
        if (index >= 0) {
          self.saved.splice(index, 1);
        }
      }
    };

    return {
      afterCreate,
      createOrUpdate,
      remove,
      toggleSave,
      beforeDestroy,
    };
  })
  .views((self) => ({
    get root() {
      return getRoot(self);
    },
  }));

export default t.compose(PostStore, makeEditable());
