import {types as t} from 'mobx-state-tree';
import AppStore from './AppStore';
import UserStore from './UserStore';
import PostStore from './PostStore';
import PostLikeStore from './PostLikeStore';

export default t
  .model('RootStore', {
    appStore: AppStore,
    userStore: UserStore,
    postLikeStore: PostLikeStore,
    postStore: PostStore,
  })
  .create({
    appStore: AppStore.create(),
    userStore: UserStore.create(),
    postLikeStore: PostLikeStore.create(),
    postStore: PostStore.create(),
  });
