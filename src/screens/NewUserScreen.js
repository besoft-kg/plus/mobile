import React from 'react';
import {Colors, TextInput, Title} from 'react-native-paper';
import {
  StatusBar,
  ScrollView,
  SafeAreaView,
  View,
  Platform,
  Keyboard,
  Alert,
} from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import {Avatar} from 'react-native-paper';
import GradientButton from 'react-native-gradient-buttons';
import {observable} from 'mobx';
import {inject, observer} from 'mobx-react';
import BaseScreen from './BaseScreen';
import Display from 'react-native-display';
import requester from '../utils/requester';

@inject('store')
@observer
class NewUserScreen extends BaseScreen {
  @observable keyboard_shown = false;
  @observable full_name = '';
  @observable login = '';
  @observable loading = false;

  componentDidMount() {
    this.keyboard_did_show_listener = Keyboard.addListener(
      Platform.select({android: 'keyboardDidShow', ios: 'keyboardWillShow'}),
      () => this.setValue('keyboard_shown', true),
    );
    this.keyboard_did_hide_listener = Keyboard.addListener(
      Platform.select({android: 'keyboardDidHide', ios: 'keyboardWillHide'}),
      () => this.setValue('keyboard_shown', false),
    );
  }

  componentWillUnmount() {
    this.keyboard_did_show_listener.remove();
    this.keyboard_did_hide_listener.remove();
  }

  setLogin = (login) => {
    if (/^[A-Za-z0-9\_]+$/.test(login) || login.length === 0) {
      this.setValue('login', login, 50);
    }
  };

  send = () => {
    if (this.loading) {
      return;
    }
    this.setValue('loading', true);
    requester
      .post('profile', {
        login: this.login.toLowerCase(),
        full_name: this.full_name,
      })
      .then(({data}) => {
        const {user} = data.payload;
        this.appStore.signIn(user);
      })
      .catch((e) => {
        console.dir(e);
        if ('response' in e && e.response.data.status === 'login_is_taken') {
          Alert.alert('Сорри!', 'Этот логин уже занят 😕');
          this.setLogin('');
        }
      })
      .finally(() => {
        this.setValue('loading', false);
      });
  };

  render() {
    return (
      <>
        <StatusBar translucent backgroundColor={'rgba(0, 0, 0, 0)'} />
        <SafeAreaView
          flex={1}
          style={{
            backgroundColor: Colors.grey200,
            marginTop: StatusBar.currentHeight,
          }}>
          <ScrollView
            contentContainerStyle={{flex: 1}}
            keyboardShouldPersistTaps={'always'}>
            <Title
              onPress={() => this.appStore.signOut()}
              style={{
                textAlign: 'center',
                margin: 8,
                marginTop: 30,
                color: Colors.grey600,
              }}>
              👋 Добро пожаловать
              {this.full_name.length > 0 ? `, ${this.full_name}` : null}!
            </Title>
            <View
              style={{
                flex: 1,
                flexDirection: 'column',
                justifyContent: this.keyboard_shown ? 'flex-end' : 'center',
              }}>
              <View
                style={{
                  flexDirection: 'column',
                  alignItems: 'center',
                  marginBottom: 40,
                }}>
                <Display
                  exit="fadeOutDown"
                  enter="fadeInUp"
                  enable={!this.keyboard_shown}>
                  <Avatar.Image
                    size={170}
                    source={require('../assets/avatar.jpg')}
                  />
                </Display>
              </View>

              <View style={{marginBottom: 10, paddingHorizontal: 30}}>
                <TextInput
                  style={{marginBottom: 10}}
                  label="🤔 Придумайте логин"
                  value={this.login}
                  onChangeText={(e) => this.setLogin(e)}
                />
                <TextInput
                  label="Полное имя"
                  value={this.full_name}
                  onChangeText={(e) => this.setValue('full_name', e, 250)}
                />
              </View>

              <Display
                exit="fadeOutDown"
                enter="fadeInUp"
                enable={
                  this.login.length > 0 &&
                  this.full_name.length > 0 &&
                  !this.loading
                }>
                <GradientButton
                  style={{marginVertical: 8, alignSelf: 'center'}}
                  text="Далее 👍"
                  textStyle={{fontSize: 20}}
                  gradientBegin="#11998e"
                  gradientEnd="#38ef7d"
                  gradientDirection="diagonal"
                  height={60}
                  width={'80%'}
                  radius={30}
                  onPressAction={() => this.send()}
                />
              </Display>

              {Platform.OS === 'ios' && <KeyboardSpacer />}
            </View>
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }
}

export default NewUserScreen;
