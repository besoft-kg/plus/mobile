import React from 'react';
import {StatusBar, FlatList, View} from 'react-native';
import {inject, observer} from 'mobx-react';
import BaseScreen from '../screens/BaseScreen';
import {observable} from 'mobx';
import requester from '../utils/requester';
import {moderateScale, verticalScale} from 'react-native-size-matters';
import PostItemComponent from '../components/PostItemComponent';
import {getSnapshot} from 'mobx-state-tree';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {Colors, Text} from 'react-native-paper';

@inject('store')
@observer
class SavedPostsScreen extends BaseScreen {
  @observable fetching_items = false;

  componentDidMount() {
    this.fetchItems().then();
  }

  fetchItems = async () => {
    if (this.fetching_items) {
      return;
    }
    this.setValue('fetching_items', true);
    try {
      const {data} = await requester.get('profile/saved', {
        ids: getSnapshot(this.postStore.saved),
      });
      this.postStore.createOrUpdate(data.payload);
      this.postStore.setValue(
        'saved',
        data.payload.map((v) => v.id),
      );
    } catch (e) {
      console.dir(e);
    } finally {
      this.setValue('fetching_items', false);
    }
  };

  render() {
    return (
      <>
        <StatusBar translucent backgroundColor={'rgba(0, 0, 0, 0)'} />
        <FlatList
          style={{
            padding: moderateScale(8),
          }}
          contentContainerStyle={{flex: 1}}
          onRefresh={() => this.fetchItems()}
          keyExtractor={(item, index) => index.toString()}
          refreshing={this.fetching_items}
          ListFooterComponent={
            <View style={{marginBottom: verticalScale(24)}} />
          }
          ListEmptyComponent={
            <View flex={1} justifyContent={'center'} alignItems={'center'}>
              <MaterialCommunityIcons
                color={Colors.grey600}
                size={100}
                name={'bookmark-outline'}
              />
              <Text style={{fontSize: 16, marginTop: 8}}>Список пуст.</Text>
            </View>
          }
          renderItem={({item, key}) => (
            <PostItemComponent item={item} key={key} />
          )}
          data={this.postStore.saved.sort(
            (a, b) => b.created_at - a.created_at,
          )}
        />
      </>
    );
  }
}

export default SavedPostsScreen;
