import React, {createRef} from 'react';
import {observable, values} from 'mobx';
import requester from '../utils/requester';
import {
  TouchableOpacity,
  FlatList,
  KeyboardAvoidingView,
  Keyboard,
  View,
  SafeAreaView,
  Platform,
  NativeModules,
  NativeEventEmitter,
} from 'react-native';
import {inject, observer, Observer} from 'mobx-react';
import BaseScreen from './BaseScreen';
import {moderateScale, verticalScale, scale} from 'react-native-size-matters';
import {
  Avatar,
  Card,
  Colors,
  IconButton,
  Paragraph,
  TextInput,
  Text,
  Divider,
} from 'react-native-paper';
import Display from 'react-native-display';

const {StatusBarManager} = NativeModules;

@inject('store')
@observer
class PostViewScreen extends BaseScreen {
  @observable content = '';
  @observable fetching = false;

  state = {statusBarHeight: 0};
  statusBarListener = null;

  keyboardDidShowSubscription = null;
  flatListRef = createRef();

  fetchItem = async (post_id) => {
    if (this.fetching) {
      return;
    }
    this.setValue('fetching', false);
    try {
      const {data} = await requester.get('post', {id: post_id});
      this.postStore.createOrUpdate(data.payload);
    } catch (e) {
      console.dir(e);
    } finally {
      this.setValue('fetching', false);
    }
  };

  componentDidMount() {
    const item = this.postStore.items.get(this.props.route.params.id);
    this.fetchItem(item.id).then();
    this.keyboardDidShowSubscription = Keyboard.addListener(
      'keyboardDidShow',
      () => {
        setTimeout(() => this.flatListRef.current?.scrollToEnd(), 100);
      },
    );

    if (Platform.OS === 'ios') {
      StatusBarManager.getHeight((statusBarFrameData) => {
        this.setState({statusBarHeight: statusBarFrameData.height});
      });
      const emitter = new NativeEventEmitter(StatusBarManager);
      this.statusBarListener = emitter.addListener(
        'statusBarFrameWillChange',
        (statusBarData) => {
          this.setState({statusBarHeight: statusBarData.frame.height});
        },
      );
    }
  }

  componentWillUnmount() {
    this.keyboardDidShowSubscription?.remove();
    this.statusBarListener?.remove();
  }

  sendComment = async () => {
    if (!this.content) {
      return;
    }
    const item = this.postStore.items.get(this.props.route.params.id);
    item.postComment(this.content, this.appStore.user.id).then();
    this.setValue('content', '');
    setTimeout(() => this.flatListRef.current?.scrollToEnd(), 100);
    //Keyboard.dismiss();
  };

  render() {
    const item = this.postStore.items.get(this.props.route.params.id);
    const user = this.userStore.items.get(item.user_id);
    const comments = values(item.comments);

    return (
      <KeyboardAvoidingView
        keyboardVerticalOffset={44 + this.state.statusBarHeight}
        flex={1}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <SafeAreaView flex={1}>
          <FlatList
            data={comments.sort((a, b) => a.created_at - b.created_at)}
            ref={this.flatListRef}
            style={{padding: moderateScale(8)}}
            keyExtractor={(s) => s.id.toString()}
            ItemSeparatorComponent={() => <Divider style={{margin: scale(4)}} />}
            renderItem={({item: v, index: i}) => {
              const u = this.userStore.items.get(v.user_id);
              return (
                <View
                  key={i}
                  style={{
                    backgroundColor: v.is_not_sync ? Colors.green50 : '',
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingBottom:
                      i === comments.length - 1 ? moderateScale(16) : 0,
                  }}>
                  <Avatar.Image
                    style={{
                      margin: moderateScale(4),
                      marginRight: moderateScale(6),
                    }}
                    size={30}
                    source={u.picture_source}
                  />
                  <View>
                    <Text>
                      {u.full_name} (@{u.login})
                    </Text>
                    <Text style={{color: Colors.grey700}}>{v.content}</Text>
                  </View>
                </View>
              );
            }}
            ListEmptyComponent={
              <Paragraph style={{padding: moderateScale(8)}}>
                ☹️ Нет комментариев...
              </Paragraph>
            }
            ListHeaderComponent={
              <>
                <Card style={{marginBottom: verticalScale(8)}}>
                  <TouchableOpacity
                    activeOpacity={0.7}
                    onPress={() =>
                      this.navigation.navigate('ProfileViewScreen', {
                        id: user.id,
                      })
                    }>
                    <Card.Title
                      title={user.full_name}
                      subtitle={`@${user.login} | 1.2 км | час назад`}
                      left={(props) => (
                        <Observer>
                          {() => (
                            <Avatar.Image
                              source={user.picture_source}
                              {...props}
                            />
                          )}
                        </Observer>
                      )}
                    />
                  </TouchableOpacity>
                  {/*<Card.Cover source={{uri: 'https://picsum.photos/700'}} />*/}
                  {item.content && (
                    <View style={{padding: verticalScale(8)}}>
                      <Paragraph>{item.content}</Paragraph>
                    </View>
                  )}
                </Card>
                <Card style={{marginBottom: verticalScale(8)}}>
                  <Paragraph style={{padding: moderateScale(8)}}>
                    👍 Лайки: {item.likes_count}
                  </Paragraph>
                </Card>
                {/*<Card style={{marginBottom: verticalScale(16)}}>*/}
                {/*  */}
                {/*</Card>*/}
              </>
            }
          />
          <View flexDirection={'row'} alignItems={'center'}>
            <TextInput
              style={{
                flexGrow: 1,
                backgroundColor: Colors.grey300,
                paddingRight: scale(34),
              }}
              placeholder={'Напишите комментарий'}
              value={this.content}
              onChangeText={(e) => this.setValue('content', e)}
            />
            <Display
              exit="fadeOutRight"
              enter="fadeInRight"
              style={{
                position: 'absolute',
                padding: moderateScale(4),
                right: scale(8),
              }}
              enable={this.content.length > 0}>
              <IconButton
                icon="send"
                size={20}
                onPress={() => this.sendComment()}
              />
            </Display>
          </View>
        </SafeAreaView>
      </KeyboardAvoidingView>
    );
  }
}

export default PostViewScreen;
