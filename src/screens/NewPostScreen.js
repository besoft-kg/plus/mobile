import React, {createRef} from 'react';
import {Title, TextInput, Colors} from 'react-native-paper';
import {
  StatusBar,
  ScrollView,
  Platform,
  PermissionsAndroid,
  Alert,
  SafeAreaView,
  KeyboardAvoidingView,
  TouchableOpacity,
  View,
} from 'react-native';
import {inject, observer} from 'mobx-react';
import BaseScreen from '../screens/BaseScreen';
import {verticalScale, moderateScale} from 'react-native-size-matters';
import {observable} from 'mobx';
import GradientButton from 'react-native-gradient-buttons';
import requester from '../utils/requester';
import Geolocation from 'react-native-geolocation-service';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

@inject('store')
@observer
class NewPostScreen extends BaseScreen {
  @observable content = '';
  @observable loading = false;

  navigationFocusListenerDisposer = null;
  navigationBlurListenerDisposer = null;
  contentRef = createRef();

  send = async () => {
    if (this.loading || this.content.length < 1) {
      return;
    }
    this.setValue('loading', true);

    try {
      const result =
        Platform.OS === 'ios'
          ? await Geolocation.requestAuthorization('whenInUse')
          : (await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
              {
                title:
                  'Нам нужен доступ к GPS, чтобы другие пользователи могли видеть расстояние между вами',
                message:
                  'Мы гарантируем, что никто никогда не сможет увидеть ваше местоположение. Пользователи видят только расстояния.',
              },
            )) === PermissionsAndroid.RESULTS.GRANTED
          ? 'granted'
          : 'denied';

      console.log('gps result:', result);

      if (result === 'granted') {
        Geolocation.getCurrentPosition(
          async ({coords}) => {
            this.appStore.user.setValues({
              location_lat: coords.latitude,
              location_lng: coords.longitude,
            });
            const {data} = await requester.post('post', {
              content: this.content,
              location_lat: coords.latitude,
              location_lng: coords.longitude,
            });
            this.setValue('loading', false);
            this.postStore.createOrUpdate(data.payload);
            this.postStore.setValue('home', [
              data.payload.id,
              ...this.postStore.home,
            ]);
            this.setValue('content', '');
            this.navigation.goBack();
          },
          (error) => {
            console.log(error.code, error.message);
            this.setValue('loading', false);
          },
          {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
        );
      } else {
        Alert.alert('Ошибка!', 'В доступе отказано!');
        this.setValue('loading', false);
      }
    } catch (e) {
      console.dir(e);
      Alert.alert('Ошибка!', e.message);
      this.setValue('loading', false);
    }
  };

  componentDidMount() {
    this.navigationFocusListenerDisposer = this.navigation.addListener(
      'focus',
      () => this.contentRef.current?.focus(),
    );
    this.navigationBlurListenerDisposer = this.navigation.addListener(
      'blur',
      () => this.contentRef.current?.blur(),
    );
  }

  componentWillUnmount() {
    this.navigationFocusListenerDisposer &&
      this.navigationFocusListenerDisposer();
    this.navigationBlurListenerDisposer &&
      this.navigationBlurListenerDisposer();
  }

  render() {
    return (
      <>
        <StatusBar translucent backgroundColor={'rgba(0, 0, 0, 0)'} />
        <KeyboardAvoidingView
          flex={1}
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
          <SafeAreaView
            flex={1}
            style={{
              backgroundColor: Colors.grey200,
              marginTop: StatusBar.currentHeight,
            }}>
            <ScrollView
              keyboardShouldPersistTaps={'always'}
              contentContainerStyle={{
                flex: 1,
                justifyContent: 'flex-end',
                alignItems: 'stretch',
                paddingBottom: verticalScale(12),
              }}>
              <Title style={{textAlign: 'center'}}>
                Напишите что-нибудь 😊
              </Title>
              <TextInput
                ref={this.contentRef}
                multiline={true}
                placeholder={'Напишите что-нибудь'}
                numberOfLines={5}
                style={{margin: moderateScale(8)}}
                value={this.content}
                disabled={this.loading}
                onChangeText={(text) => this.setValue('content', text)}
              />
              <View
                justifyContent={'center'}
                alignItems={'center'}
                flexDirection={'row'}>
                <GradientButton
                  style={{marginVertical: 8, alignSelf: 'center'}}
                  text="Опубликовать 🤟"
                  textStyle={{fontSize: 20}}
                  gradientBegin="#4568DC"
                  gradientEnd="#B06AB3"
                  gradientDirection="diagonal"
                  height={50}
                  disabled={this.loading || this.content.length < 1}
                  width={'80%'}
                  radius={30}
                  onPressAction={() => this.send()}
                />
                <TouchableOpacity
                  onPress={() => this.navigation.navigate('AttachMediaScreen')}
                  style={{
                    marginLeft: moderateScale(6),
                    marginRight: moderateScale(6),
                    borderRadius: moderateScale(5),
                    width: moderateScale(40),
                    height: verticalScale(40),
                    backgroundColor: Colors.green400,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <MaterialCommunityIcons
                    name={'camera'}
                    color={'#fff'}
                    size={moderateScale(30)}
                  />
                </TouchableOpacity>
              </View>
            </ScrollView>
          </SafeAreaView>
        </KeyboardAvoidingView>
      </>
    );
  }
}

export default NewPostScreen;
