import React from 'react';
import {Card, Colors, Title, Text, Divider} from 'react-native-paper';
import {StatusBar, ScrollView, View, Alert} from 'react-native';
import {inject, observer} from 'mobx-react';
import BaseScreen from '../screens/BaseScreen';
import {withTranslation} from 'react-i18next';
import {List} from 'react-native-paper';
import {APP_VERSION_NAME} from '../utils/settings';
import {verticalScale} from 'react-native-size-matters';
import ProfilePictureComponent from '../components/ProfilePictureComponent';

@withTranslation('profile')
@inject('store')
@observer
class ProfileScreen extends BaseScreen {
  confirmSignOut = () => {
    Alert.alert('Подтвердите', 'Вы точно хотите выйти?', [
      {
        text: 'Нет',
      },
      {text: 'Да', onPress: () => this.appStore.signOut()},
    ]);
  };

  render() {
    if (!this.appStore.authenticated) {
      return null;
    }

    return (
      <>
        <StatusBar translucent backgroundColor={'rgba(0, 0, 0, 0)'} />

        <ScrollView
          contentContainerStyle={{
            paddingBottom: verticalScale(50),
          }}>
          <View
            style={{
              flexDirection: 'column',
              alignItems: 'center',
              marginTop: verticalScale(60),
            }}>
            <ProfilePictureComponent />
            <Title
              onPress={() => this.appStore.signOut()}
              style={{
                textAlign: 'center',
                margin: 8,
                marginTop: 30,
                color: Colors.grey600,
              }}>
              👋 {this.appStore.user.full_name}
            </Title>
            <Title
              style={{
                textAlign: 'center',
                margin: 8,
                marginBottom: 8,
                color: Colors.grey600,
              }}>
              @{this.appStore.user.login}
            </Title>
          </View>
          <Card style={{margin: 8, marginBottom: 13}}>
            <List.Item
              title={this.t('edit_profile')}
              onPress={() =>
                this.props.navigation.navigate('EditProfileScreen')
              }
              left={(props) => <List.Icon {...props} icon="pencil" />}
            />
            <Divider />
            <List.Item
              title={this.t('my_posts')}
              onPress={() => this.navigation.navigate('MyPostsScreen')}
              left={(props) => <List.Icon {...props} icon="menu" />}
            />
            <Divider />
            <List.Item
              title={this.t('liked_posts')}
              onPress={() => this.navigation.navigate('LikedPostsScreen')}
              left={(props) => <List.Icon {...props} icon="heart-outline" />}
            />
            <Divider />
            <List.Item
              title={this.t('saved_posts')}
              onPress={() => this.navigation.navigate('SavedPostsScreen')}
              left={(props) => <List.Icon {...props} icon="bookmark-outline" />}
            />
          </Card>

          <Card style={{margin: 8, marginBottom: 13}}>
            <List.Item
              title={this.t('invite_friends')}
              onPress={() => this.props.navigation.navigate('AboutUsScreen')}
              left={(props) => (
                <List.Icon {...props} icon="account-multiple-plus-outline" />
              )}
            />
            <Divider />
            <List.Item
              title={this.t('about_app')}
              onPress={() => this.props.navigation.navigate('AboutUsScreen')}
              left={(props) => (
                <List.Icon {...props} icon="information-outline" />
              )}
            />
            <Divider />
            <List.Item
              title={this.t('sign_out')}
              onPress={() => this.confirmSignOut()}
              left={(props) => <List.Icon {...props} icon="exit-run" />}
            />
          </Card>

          <Text style={{textAlign: 'center', color: Colors.grey600}}>
            Версия: {APP_VERSION_NAME}
          </Text>
          <Title style={{textAlign: 'center', color: Colors.grey600}}>
            🇰🇬 Made in Kyrgyzstan
          </Title>
          <Title style={{textAlign: 'center', color: Colors.grey600}}>
            by Besoft with ❤️
          </Title>
        </ScrollView>
      </>
    );
  }
}

export default ProfileScreen;
