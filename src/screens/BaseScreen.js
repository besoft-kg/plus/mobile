import React from 'react';
import {action} from 'mobx';

class BaseScreen extends React.Component {
  constructor(props) {
    super(props);

    if ('store' in this.props) {
      this.store = this.props.store;
      this.appStore = this.store.appStore;
      this.userStore = this.store.userStore;
      this.postStore = this.store.postStore;
      this.postLikeStore = this.store.postLikeStore;
    }

    if ('navigation' in this.props) {
      this.navigation = this.props.navigation;
    }
    if ('t' in this.props) {
      this.t = this.props.t;
    }
  }

  @action setValue = (name, value, max_length = null) => {
    this[name] = max_length ? value.slice(0, max_length) : value;
  };

  @action setValues = (s) => Object.keys(s).map((v) => (this[v] = s[v]));
}

export default BaseScreen;
