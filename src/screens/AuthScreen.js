import React from 'react';
import {Button, Colors, Text, TextInput, Title} from 'react-native-paper';
import {
  StatusBar,
  ScrollView,
  SafeAreaView,
  View,
  Platform,
  Alert,
} from 'react-native';
import auth from '@react-native-firebase/auth';
import GradientButton from 'react-native-gradient-buttons';
import Display from 'react-native-display';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import VerifCode from 'rn-verifcode';
import PhoneNumber from '../utils/PhoneNumber';
import BaseScreen from './BaseScreen';
import {inject, observer} from 'mobx-react';
import {observable} from 'mobx';

@inject('store')
@observer
class AuthScreen extends BaseScreen {
  verification_code_ref = React.createRef();

  @observable phone_number = '';
  @observable verification_code = '';
  @observable confirm = null;
  @observable status = 'phone_number';
  @observable phone_number_loading = false;
  @observable verification_code_loading = false;
  @observable user_checking = false;

  constructor(props) {
    super(props);

    this.phone_number_object = new PhoneNumber();

    this.unsubscribe = () => {};
  }

  setPhoneNumber = (n) => {
    this.phone_number_object.parseFromString(n);
    this.setValue('phone_number', n);
  };

  send = async () => {
    if (this.phone_number_loading) {
      return;
    }
    try {
      this.setValue('phone_number_loading', true);
      const confirmation = await auth().signInWithPhoneNumber(
        this.phone_number_object.getE164Format(),
      );
      this.setValues({confirm: confirmation, status: 'verification_code'});
    } catch (e) {
      Alert.alert('Ошибка!', e.message);
    } finally {
      this.setValues({phone_number_loading: false});
    }
  };

  check = async (verification_code) => {
    if (this.verification_code_loading) {
      return;
    }
    try {
      this.setValues({verification_code_loading: true, verification_code});
      await this.confirm.confirm(verification_code);
    } catch (error) {
      console.dir(error);
      Alert.alert('Ошибка!', error.message);
      this.setValues({verification_code: ''});
      this.verification_code_ref.current.reset();
    } finally {
      this.setValues({
        verification_code_loading: false,
        // user_checking: false,
      });
    }
  };

  incorrectPhoneNumber = () => {
    this.setValues({
      phone_number: '',
      verification_code: '',
      confirm: null,
      status: 'phone_number',
      phone_number_loading: false,
      verification_code_loading: false,
    });
    this.phone_number_object = new PhoneNumber();
  };

  render() {
    return (
      <>
        <StatusBar translucent backgroundColor={'rgba(0, 0, 0, 0)'} />
        <SafeAreaView
          flex={1}
          style={{
            backgroundColor: Colors.grey200,
            marginTop: StatusBar.currentHeight,
          }}>
          <ScrollView
            keyboardShouldPersistTaps={'always'}
            contentContainerStyle={{
              flex: 1,
              justifyContent: 'space-between',
            }}>
            <Text style={{fontSize: 24, marginTop: 12, textAlign: 'center'}}>
              Besoft ➕
            </Text>
            {this.status === 'phone_number' ? (
              <>
                {this.phone_number_object.isValid() ? (
                  <View>
                    <Text
                      style={{
                        fontSize: 64,
                        textAlign: 'center',
                        marginBottom: 12,
                      }}>
                      🛫
                    </Text>
                    <Text style={{fontSize: 24, textAlign: 'center'}}>
                      Нажмите на "Отправить код"
                    </Text>
                  </View>
                ) : (
                  <View>
                    <Text
                      style={{
                        fontSize: 64,
                        textAlign: 'center',
                        marginBottom: 12,
                      }}>
                      📞
                    </Text>
                    <Text style={{fontSize: 24, textAlign: 'center'}}>
                      Ваш телефон номер
                    </Text>
                  </View>
                )}
              </>
            ) : (
              <>
                {this.verification_code_loading ? (
                  <View>
                    <Text style={{fontSize: 24, textAlign: 'center'}}>
                      {this.user_checking
                        ? '🤫 Еще чуть-чуть...'
                        : '⏳ Подождите...'}
                    </Text>
                    <Text
                      style={{fontSize: 18, marginTop: 8, textAlign: 'center'}}>
                      Мы проверяем ваш код
                    </Text>
                  </View>
                ) : (
                  <View>
                    <Text style={{fontSize: 24, textAlign: 'center'}}>
                      Введите код подтверждения
                    </Text>
                    <Text
                      style={{fontSize: 18, marginTop: 8, textAlign: 'center'}}>
                      Мы отправили код подтверждения на номер
                    </Text>
                  </View>
                )}
              </>
            )}
            <View style={{margin: 12}}>
              {this.status === 'verification_code' ? (
                <>
                  <Text
                    style={{
                      fontSize: 24,
                      marginBottom: 8,
                      textAlign: 'center',
                    }}>
                    {this.phone_number_object.getE164Format()}
                  </Text>
                  <Button
                    disabled={this.verification_code_loading}
                    style={{alignSelf: 'center'}}
                    onPress={() => this.incorrectPhoneNumber()}>
                    Неверный номер?
                  </Button>
                </>
              ) : (
                <TextInput
                  keyboardType={'phone-pad'}
                  label={'Телефон номер'}
                  disabled={this.phone_number_loading}
                  value={this.phone_number}
                  style={{margin: 8}}
                  onChangeText={(n) => this.setPhoneNumber(n)}
                />
              )}
              <Display
                exit="fadeOutDown"
                enter="fadeInUp"
                style={{alignSelf: 'center'}}
                enable={this.status === 'verification_code'}>
                <VerifCode
                  ref={this.verification_code_ref}
                  autofocus
                  onFulfill={(code) => this.check(code)}
                />
              </Display>
              <Display
                exit="fadeOutDown"
                enter="fadeInUp"
                enable={
                  this.phone_number_object.isValid() &&
                  this.status === 'phone_number' &&
                  !this.phone_number_loading
                }>
                <GradientButton
                  style={{marginVertical: 8, alignSelf: 'center'}}
                  text="Отправить код"
                  textStyle={{fontSize: 20}}
                  gradientBegin="#11998e"
                  gradientEnd="#38ef7d"
                  gradientDirection="diagonal"
                  height={60}
                  width={'80%'}
                  radius={30}
                  onPressAction={() => this.send()}
                />
              </Display>
              <Display
                exit="fadeOutDown"
                enter="fadeInUp"
                enable={
                  this.verification_code?.length === 6 &&
                  this.status === 'verification_code' &&
                  !this.verification_code_loading
                }>
                <GradientButton
                  style={{marginVertical: 8, alignSelf: 'center'}}
                  text="Подтвердить код"
                  textStyle={{fontSize: 20}}
                  gradientBegin="#134E5E"
                  gradientEnd="#71B280"
                  gradientDirection="diagonal"
                  height={60}
                  width={'80%'}
                  radius={30}
                  onPressAction={() => this.check()}
                />
              </Display>
            </View>
            <View>
              <Title style={{textAlign: 'center'}}>🇰🇬 Made in Kyrgyzstan</Title>
              <Text
                style={{textAlign: 'center', margin: 8, color: Colors.grey500}}>
                Нажимая на кнопку «Отправить код», вы соглашаетесь с политикой конфиденциальности и использованием пользовательских данных.
              </Text>
              {Platform.OS === 'ios' && <KeyboardSpacer />}
            </View>
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }
}

export default AuthScreen;
