import React from 'react';
import {Colors} from 'react-native-paper';
import {TouchableOpacity, View, Platform} from 'react-native';
import {inject, observer} from 'mobx-react';
import BaseScreen from './BaseScreen';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import HomeStack from '../stacks/HomeStack';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {getBottomSpace} from 'react-native-iphone-x-helper';
import NewPostStack from '../stacks/NewPostStack';
import {moderateScale, verticalScale} from 'react-native-size-matters';
import ProfileStack from '../stacks/ProfileStack';
import NotificationsStack from '../stacks/NotificationsStack';
import DiscoverStack from '../stacks/DiscoverStack';

const Tab = createBottomTabNavigator();

function CustomTabBar({state, descriptors, navigation}) {
  const focusedOptions = descriptors[state.routes[state.index].key].options;

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  return (
    <View
      style={{
        flexDirection: 'row',
        shadowOffset: {
          width: 0,
          height: -3,
        },
        shadowOpacity: 0.18,
        shadowRadius: 1.0,
        elevation: 1,
      }}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            activeOpacity={1}
            accessibilityState={isFocused ? {selected: true} : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            key={index}
            onLongPress={onLongPress}
            style={[
              {
                flexGrow: route.name === 'NewPostStack' ? 1.6 : 1,
                height: verticalScale(48),
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: '#fff',
              },
              Platform.OS === 'ios' ? {} : {elevation: 24},
            ]}>
            {route.name === 'NewPostStack' ? (
              <View
                style={{
                  position: 'absolute',
                  top: verticalScale(-8),
                  backgroundColor: '#fff',
                  borderRadius: 100,
                  shadowOffset: {
                    width: 0,
                    height: 0,
                  },
                  shadowOpacity: 0.2,
                  shadowRadius: 1.0,
                  elevation: 5,
                }}>
                {options.tabBarIcon({
                  size: moderateScale(45),
                  focused: isFocused,
                  color: isFocused ? Colors.green700 : Colors.grey500,
                })}
              </View>
            ) : (
              <>
                {options.tabBarIcon({
                  size: moderateScale(32),
                  focused: isFocused,
                  color: isFocused ? Colors.green700 : Colors.grey500,
                })}
              </>
            )}
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

@inject('store')
@observer
class MainScreen extends BaseScreen {
  render() {
    return (
      <>
        <View
          flex={1}
          style={{
            backgroundColor: Colors.white,
            paddingBottom: getBottomSpace() - 15,
          }}>
          <Tab.Navigator
            tabBar={(props) => <CustomTabBar {...props} />}
            tabBarOptions={{
              showLabel: false,
            }}>
            <Tab.Screen
              name="HomeStack"
              component={HomeStack}
              options={{
                tabBarIcon: ({color, size}) => (
                  <MaterialCommunityIcons
                    name="home"
                    color={color}
                    size={size}
                  />
                ),
              }}
            />
            <Tab.Screen
              name="DiscoverStack"
              component={DiscoverStack}
              options={{
                tabBarIcon: ({color, size}) => (
                  <MaterialCommunityIcons
                    name="magnify"
                    color={color}
                    size={size}
                  />
                ),
              }}
            />
            <Tab.Screen
              name="NewPostStack"
              component={NewPostStack}
              options={{
                tabBarIcon: ({color, size}) => (
                  <MaterialCommunityIcons
                    name="plus-circle"
                    color={color}
                    size={size + moderateScale(20)}
                  />
                ),
              }}
            />
            <Tab.Screen
              name="NotificationsStack"
              component={NotificationsStack}
              options={{
                tabBarBadge: 3,
                tabBarIcon: ({color, size, focused}) => (
                  <MaterialCommunityIcons
                    name={focused ? 'cards-heart' : 'heart-outline'}
                    color={color}
                    size={size}
                  />
                ),
              }}
            />
            <Tab.Screen
              name="ProfileStack"
              component={ProfileStack}
              options={{
                tabBarIcon: ({color, size}) => (
                  <MaterialCommunityIcons
                    name="account"
                    color={color}
                    size={size}
                  />
                ),
              }}
            />
          </Tab.Navigator>
        </View>
      </>
    );
  }
}

export default MainScreen;
