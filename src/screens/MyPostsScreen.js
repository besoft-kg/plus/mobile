import React from 'react';
import {StatusBar, FlatList, View} from 'react-native';
import {inject, observer} from 'mobx-react';
import BaseScreen from '../screens/BaseScreen';
import {observable} from 'mobx';
import requester from '../utils/requester';
import {moderateScale, verticalScale} from 'react-native-size-matters';
import PostItemComponent from '../components/PostItemComponent';

@inject('store')
@observer
class MyPostsScreen extends BaseScreen {
  @observable fetching_items = false;

  componentDidMount() {
    this.fetchItems().then();
  }

  fetchItems = async () => {
    if (this.fetching_items) {
      return;
    }
    this.setValue('fetching_items', true);
    try {
      const {data} = await requester.get('profile/post');
      this.postStore.createOrUpdate(data.payload);
      this.appStore.user.setValue(
        'posts',
        data.payload.map((v) => v.id),
      );
    } catch (e) {
      console.dir(e);
    } finally {
      this.setValue('fetching_items', false);
    }
  };

  render() {
    return (
      <>
        <StatusBar translucent backgroundColor={'rgba(0, 0, 0, 0)'} />
        <FlatList
          style={{
            padding: moderateScale(8),
          }}
          onRefresh={() => this.fetchItems()}
          keyExtractor={(item, index) => index.toString()}
          refreshing={this.fetching_items}
          ListFooterComponent={
            <View style={{marginBottom: verticalScale(24)}} />
          }
          renderItem={({item, key}) => (
            <PostItemComponent item={item} key={key} />
          )}
          data={this.appStore.user.posts.sort(
            (a, b) => b.created_at - a.created_at,
          )}
        />
      </>
    );
  }
}

export default MyPostsScreen;
