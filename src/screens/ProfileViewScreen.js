import React from 'react';
import {Card, Colors, Paragraph, Text, Title} from 'react-native-paper';
import {StatusBar, View, FlatList} from 'react-native';
import {inject, observer} from 'mobx-react';
import BaseScreen from '../screens/BaseScreen';
import {withTranslation} from 'react-i18next';
import {Avatar} from 'react-native-paper';
import PostItemComponent from '../components/PostItemComponent';
import {observable} from 'mobx';
import requester from '../utils/requester';
import {moderateScale, verticalScale} from 'react-native-size-matters';

@withTranslation('profile')
@inject('store')
@observer
class ProfileViewScreen extends BaseScreen {
  @observable fetching_posts = false;

  componentDidMount() {
    const item = this.userStore.items.get(this.props.route.params.id);
    this.navigation.setOptions({
      title: item.full_name,
    });
    this.fetchPosts().then();
  }

  fetchPosts = async () => {
    if (this.fetching_posts) {
      return;
    }
    const item = this.userStore.items.get(this.props.route.params.id);
    this.setValue('fetching_posts', true);
    try {
      const {data} = await requester.get('post', {
        user_id: this.props.route.params.id,
      });
      this.postStore.createOrUpdate(data.payload);
      item.setValue(
        'posts',
        data.payload.map((k) => k.id),
      );
      console.log(data.payload);
    } catch (e) {
      console.dir(e);
    } finally {
      this.setValue('fetching_posts', false);
    }
  };

  render() {
    const item = this.userStore.items.get(this.props.route.params.id);

    return (
      <>
        <StatusBar translucent backgroundColor={'rgba(0, 0, 0, 0)'} />
        <FlatList
          contentContainerStyle={{
            margin: moderateScale(8),
            paddingTop: verticalScale(60),
            paddingBottom: moderateScale(8),
          }}
          ListHeaderComponent={
            <View
              style={{
                flexDirection: 'column',
                alignItems: 'center',
              }}>
              <Avatar.Image size={120} source={item.picture_source} />
              <Title
                style={{
                  textAlign: 'center',
                  margin: 8,
                  marginTop: 30,
                  color: Colors.grey600,
                }}>
                👋 {item.full_name}
              </Title>
              <Title
                style={{
                  textAlign: 'center',
                  margin: 8,
                  marginBottom: 8,
                  color: Colors.grey600,
                }}>
                @{item.login}
              </Title>
            </View>
          }
          ListEmptyComponent={
            this.fetching_posts ? null : (
              <Card
                style={{
                  paddingTop: verticalScale(16),
                  paddingBottom: verticalScale(16),
                }}>
                <Text style={{fontSize: 44, textAlign: 'center'}}>☹</Text>
                <Paragraph
                  style={{textAlign: 'center', padding: moderateScale(8)}}>
                  Нет ни одного поста...
                </Paragraph>
              </Card>
            )
          }
          keyExtractor={(s) => s.id.toString()}
          onRefresh={() => this.fetchPosts()}
          refreshing={this.fetching_posts}
          renderItem={({item: _item, key}) => (
            <PostItemComponent item={_item} key={key} />
          )}
          data={item.posts.sort((a, b) => b.created_at - a.created_at)}
        />
      </>
    );
  }
}

export default ProfileViewScreen;
