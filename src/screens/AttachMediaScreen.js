import React from 'react';
import BaseScreen from '../screens/BaseScreen';
import InDevelopmentComponent from '../components/InDevelopmentComponent';

class AttachMediaScreen extends BaseScreen {
  render() {
    return <InDevelopmentComponent />;
  }
}

export default AttachMediaScreen;
