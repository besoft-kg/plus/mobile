import React from 'react';
import {Colors, Text, Title} from 'react-native-paper';
import {
  StatusBar,
  View,
  FlatList,
  Platform,
  PermissionsAndroid,
} from 'react-native';
import {inject, observer} from 'mobx-react';
import BaseScreen from '../screens/BaseScreen';
import {getStatusBarHeight} from 'react-native-iphone-x-helper';
import {APP_NAME} from '../utils/settings';
import {verticalScale, moderateScale, scale} from 'react-native-size-matters';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import PostItemComponent from '../components/PostItemComponent';
import {observable} from 'mobx';
import requester from '../utils/requester';
import Geolocation from 'react-native-geolocation-service';
import RNPickerSelect from 'react-native-picker-select';
import storage from '../utils/storage';

const TOP_SPACE = getStatusBarHeight() + verticalScale(12);

@inject('store')
@observer
class HomeScreen extends BaseScreen {
  @observable fetching_posts = false;

  componentDidMount() {
    this.fetchPosts(this.appStore.distance).then();
  }

  setDistance = (i) => {
    this.appStore.setValue('distance', i);
    this.fetchPosts(i).then();
    storage.set('distance', i).then();
  };

  makeRequest = async (distance = 0, coords = null) => {
    const {data} = await requester.get('post', {
      distance,
      ...(coords
        ? {
            location_lat: coords.latitude,
            location_lng: coords.longitude,
          }
        : {}),
    });
    this.postStore.createOrUpdate(data.payload);
    this.postStore.setValue(
      'home',
      data.payload.map((v) => v.id),
    );
  };

  fetchPosts = async (distance = 0) => {
    if (this.fetching_posts) {
      return;
    }
    this.setValue('fetching_posts', true);
    try {
      if (distance === 0) {
        this.makeRequest(distance).then();
      } else {
        const result =
          Platform.OS === 'ios'
            ? await Geolocation.requestAuthorization('whenInUse')
            : (await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                  title:
                    'Нам нужен доступ к GPS, чтобы фильтровать посты по расстоянию',
                  message:
                    'Мы гарантируем, что никто никогда не сможет увидеть ваше местоположение. Пользователи видят только расстояния.',
                },
              )) === PermissionsAndroid.RESULTS.GRANTED
            ? 'granted'
            : 'denied';

        if (result === 'granted') {
          Geolocation.getCurrentPosition(
            async ({coords}) => {
              this.appStore.user.setValues({
                location_lat: coords.latitude,
                location_lng: coords.longitude,
              });
              this.makeRequest(distance, coords).then();
            },
            (error) => {
              console.log(error.code, error.message);
              this.makeRequest(distance).then();
              this.appStore.setValue('distance', 0);
              storage.set('distance', 0).then();
            },
            {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
          );
        } else {
          this.makeRequest(distance).then();
          this.appStore.setValue('distance', 0);
          storage.set('distance', 0).then();
        }
      }
    } catch (e) {
      console.dir(e);
    } finally {
      this.setValue('fetching_posts', false);
    }
  };

  render() {
    return (
      <>
        <StatusBar translucent backgroundColor={'rgba(0, 0, 0, 0)'} />
        <FlatList
          style={{
            padding: moderateScale(8),
          }}
          onRefresh={() => this.fetchPosts(this.appStore.distance)}
          keyExtractor={(item, index) => index.toString()}
          refreshing={this.fetching_posts}
          ListFooterComponent={
            <View style={{marginBottom: verticalScale(24)}} />
          }
          ListHeaderComponent={
            <View
              style={{
                marginTop: TOP_SPACE,
                marginBottom: verticalScale(8),
              }}
              justifyContent={'space-between'}
              flexDirection={'row'}>
              <View>
                <Title
                  style={{
                    margin: scale(8),
                    color: Colors.grey600,
                  }}>
                  {APP_NAME}
                </Title>
              </View>
              <View flexDirection={'row'} alignItems={'center'}>
                <RNPickerSelect
                  value={this.appStore.distance}
                  onValueChange={(value) => this.setDistance(value)}
                  items={[
                    {label: '~ км', value: 0},
                    {label: '1 км', value: 1},
                    {label: '5 км', value: 5},
                    {label: '10 км', value: 10},
                    {label: '50 км', value: 50},
                    {label: '100 км', value: 100},
                  ]}
                  placeholder={{}}>
                  <View
                    style={{
                      backgroundColor: Colors.white,
                      borderColor: Colors.grey50,
                      borderWidth: 1,
                      borderRadius: 10,
                      shadowColor: '#000',
                      shadowOffset: {
                        width: 0,
                        height: 1,
                      },
                      shadowOpacity: 0.18,
                      shadowRadius: 1.0,
                      elevation: 1,
                      paddingHorizontal: moderateScale(4),
                      marginRight: scale(8),
                    }}
                    flexDirection={'row'}
                    alignItems={'center'}>
                    <MaterialCommunityIcons
                      size={moderateScale(24)}
                      color={Colors.green400}
                      name={'map-marker'}
                    />
                    <Text
                      style={{
                        fontSize: moderateScale(18),
                        color: Colors.green400,
                      }}>
                      {this.appStore.distance === 0
                        ? '~'
                        : this.appStore.distance}{' '}
                      км
                    </Text>
                  </View>
                </RNPickerSelect>
                {/*<Text*/}
                {/*  onPress={() =>*/}
                {/*    this.appStore.setLanguage(*/}
                {/*      this.appStore.language === 'ru' ? 'ky' : 'ru',*/}
                {/*    )*/}
                {/*  }*/}
                {/*  style={{*/}
                {/*    marginHorizontal: scale(8),*/}
                {/*    fontSize: moderateScale(28),*/}
                {/*  }}>*/}
                {/*  {this.appStore.language === 'ky' ? '🇰🇬' : '🇷🇺'}*/}
                {/*</Text>*/}
              </View>
            </View>
          }
          renderItem={({item, key}) => (
            <PostItemComponent item={item} key={key} />
          )}
          data={this.postStore.home.sort((a, b) => b.created_at - a.created_at)}
        />
      </>
    );
  }
}

export default HomeScreen;
