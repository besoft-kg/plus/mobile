export const APP_NAME = 'Besoft+';

export const DEBUG_MODE = process.env.NODE_ENV !== 'production';
export const WEB_URL = DEBUG_MODE
  ? 'http://localhost:3000'
  : 'https://plus.besoft.kg';
export const API_VERSION = 1;
export const APP_VERSION = 11;
export const APP_VERSION_NAME = '0.1.1';
export const API_URL = DEBUG_MODE
  ? 'http://localhost:5000'
  : 'https://api.plus.besoft.kg';
export const API_URL_WITH_VERSION = API_URL + '/v' + API_VERSION;
export const STORAGE_PREFIX = 'BesoftPlus';
