import i18next from 'i18next';
import AsyncStoragePlugin from 'i18next-react-native-async-storage';
import storage from './storage';
import {DEBUG_MODE} from './settings';

const detectUserLanguage = (callback) => {
  storage
    .get('language', 'ru')
    .then((lng) => {
      console.log(lng);
      callback(lng);
    })
    .catch((e) => callback('ru'));
};

i18next
  .use(AsyncStoragePlugin(detectUserLanguage))
  .init(
    {
      react: {
        useSuspense: false,
      },
      fallbackLng: ['ru', 'ky'],
      ns: ['app', 'profile'],
      language: 'ru',
      defaultNS: 'app',
      fallbackNS: 'app',
      debug: DEBUG_MODE,
      resources: {
        ky: {
          app: require('../locales/ky/app'),
          profile: require('../locales/ky/profile'),
        },
        ru: {
          app: require('../locales/ru/app'),
          profile: require('../locales/ru/profile'),
        },
      },
    },
    (err) => {
      if (err) {
        return console.log('something went wrong loading', err);
      }
    },
  )
  .then((r) => {});

export default i18next;
