import axios from 'axios';
import {API_URL_WITH_VERSION} from './settings';
import auth from '@react-native-firebase/auth';
import {jsonToFormData} from './index';

const axios_instance = axios.create({
  baseURL: API_URL_WITH_VERSION,
  responseType: 'json',
  responseEncoding: 'utf8',
});

axios_instance._configs = {
  silence: false,
};

const responseHandler = (response) => {
  return response;
};

const exceptionHandler = (error) => {
  return error;
};

const request = async (method, cmd, params, silence = true) => {
  const user = auth().currentUser;
  try {
    const res = await axios.request({
      url: `${API_URL_WITH_VERSION}/${cmd}`,
      [method === 'post' ? 'data' : 'params']:
        method === 'post' ? jsonToFormData(params) : params,
      method,
      headers: user ? {Authorization: `Bearer ${await user.getIdToken()}`} : {},
    });
    return responseHandler(res);
  } catch (e) {
    throw exceptionHandler(e);
  }
};

export default {
  post: (cmd, params, silence = true) => {
    return request('post', cmd, params, silence);
  },
  get: async (cmd, params, silence = true) => {
    return request('get', cmd, params, silence);
  },
};
