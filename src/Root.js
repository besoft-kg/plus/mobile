import 'react-native-gesture-handler';
import React from 'react';
import {StatusBar} from 'react-native';
import App from './App';
import {Provider as PaperProvider} from 'react-native-paper';
import {NavigationContainer} from '@react-navigation/native';
import {Provider} from 'mobx-react';
import RootStore from './stores/RootStore';
import {I18nextProvider} from 'react-i18next';
import i18next from './utils/i18next';
// import {GoogleSignin} from '@react-native-google-signin/google-signin';

// GoogleSignin.configure({
//   webClientId:
//     '935195894789-doj6uv26ct8lrbs98k3m7gccvk5icdme.apps.googleusercontent.com',
// });

const Root: () => React$Node = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <NavigationContainer>
        <I18nextProvider i18n={i18next}>
          <Provider store={RootStore}>
            <PaperProvider>
              <App />
            </PaperProvider>
          </Provider>
        </I18nextProvider>
      </NavigationContainer>
    </>
  );
};

export default Root;
