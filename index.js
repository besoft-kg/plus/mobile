/**
 * @format
 */

import {AppRegistry, LogBox} from 'react-native';
import Root from './src/Root';
import {name as appName} from './app.json';
import messaging from '@react-native-firebase/messaging';
import notifee, {AndroidColor, AndroidImportance} from '@notifee/react-native';

LogBox.ignoreLogs(['componentWillUpdate has been renamed, ']);

async function onMessageReceived(message) {
  const type = message.data?.type;
  const payload = JSON.parse(message.data?.payload);

  let channelId;

  switch (type) {
    case 'user_likes_post':
      channelId = (await notifee.getChannel('like'))
        ? 'like'
        : await notifee.createChannel({
            id: 'like',
            name: 'Лайки',
            lights: true,
            vibration: true,
            badge: true,
            importance: AndroidImportance.HIGH,
            sound: 'default',
            lightColor: AndroidColor.GREEN,
          });

      await notifee.displayNotification({
        //id: payload?.id,
        body: `@${payload?.user_login} нравится ваш пост.`,
        android: {
          channelId,
          importance: AndroidImportance.HIGH,
          sound: 'default',
          smallIcon: 'ic_notification_favorite',
          largeIcon: 'ic_launcher',
          color: AndroidColor.GREEN,
        },
      });
      break;

    case 'user_comments_post':
      channelId = (await notifee.getChannel('comment'))
        ? 'comment'
        : await notifee.createChannel({
            id: 'comment',
            name: 'Комментарии',
            lights: true,
            vibration: true,
            badge: true,
            importance: AndroidImportance.HIGH,
            sound: 'default',
            lightColor: AndroidColor.GREEN,
          });

      await notifee.displayNotification({
        //id: payload?.id,
        body: `@${payload?.user_login} прокомментировал(-а) ваш пост.`,
        android: {
          channelId,
          importance: AndroidImportance.HIGH,
          sound: 'default',
          smallIcon: 'ic_notification_comment',
          largeIcon: 'ic_launcher',
          color: AndroidColor.GREEN,
        },
      });
      break;
  }
}

messaging().onMessage(onMessageReceived);
messaging().setBackgroundMessageHandler(onMessageReceived);

AppRegistry.registerComponent(appName, () => Root);
